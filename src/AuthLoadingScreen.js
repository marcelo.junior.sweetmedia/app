/**
 * Arquivo que verifica se o usuario esta logado
 */
import React, {Component} from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import OneSignal from 'react-native-onesignal';
//IMPORT REDUX
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {saveLogin, addMsg, setOneSignalId} from './store/reducers/user';
//IMPORT API
import {apiAuth, apiMessages} from './services/api';

class AuthLoadingScreen extends Component {
    state = {
        loading: true
    }
    constructor(properties) {
        super(properties);
        OneSignal.init("5e596715-7eb0-47a1-85e6-27cb19f81255");//ID do app no OneSignal
        OneSignal.inFocusDisplaying(2);//A notificação será exibida no Notification Shade. Igual a quando o aplicativo não está em foco.
    }
    componentDidMount() {
        //Add os eventos ouvintes dos push
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
        this.automaticLogin();//Verficia se esta logado
    }
    componentWillUnmount() {
        this.setState({ loading: false });//desativa o loading quando o component é desmontado
    }
    //Atualiza o redux com as novas mensagens
    updateMessages = async() => {
        try{
            const response = await apiMessages.get(`search/${this.props.user.infoUser.id}`);
            if(response.data.success){
                this.props.addMsg(response.data.data);
            }
        }catch(error){
            console.log(error);
        }
    }
    //Atualiza a lista de mensagens quando a notificação é recebida mas nao é aberta
    onReceived = async(notification) => {
        console.log(notification);
        this.updateMessages();
    }
    //Mostra a mensagem para o usuario quando o push e clicado
    onOpened = async (openResult) => {
        // console.log('Data: ', openResult.notification.payload.additionalData);
        try {
            const email = await AsyncStorage.getItem('@Sweet_user');
            if(email !== null) {//verifica se o email esta salavo no dispositivo
                const response = await apiAuth.post(`click-login`,{email});//faz login com email
                if(response.data.status == 'success'){//Caso ocorra sucesso na conexao
                    this.props.saveLogin(response.data.data);//salva os dados do usuario no redux
                    this.props.navigation.navigate('MessageView', {message: openResult.notification.payload.additionalData})//Abre o app na tela da mensagem dentro do app
                }
            }else{//Caso o email não esteja salvo no dispositivo
                console.log(openResult.notification.payload);
                this.props.navigation.navigate('NotificationView', {message: openResult.notification.payload.additionalData})//Abre o app na tela de mensagem no Login
            }
        }catch(error){
            console.log(error);
        }
    }
    //Pega o ID do usuario assim que o aplicativo inicia
    onIds = (device) => {
        //Salva o id do onesignal no redux para ser usado no login
        this.props.setOneSignalId(device.userId);
        // console.log('User ID: ', device.userId);
    }
    //controla o login automatico
    automaticLogin = async () => {
        try {
            const email = await AsyncStorage.getItem('@Sweet_user');
            if(email !== null) {
                try{
                    const response = await apiAuth.post(`click-login`,{email});
                    if(response.data.status == 'success'){//Caso ocorra sucesso na conexao
                        this.props.saveLogin(response.data.data);
                        OneSignal.clearOneSignalNotifications();//Limpa as notificações da barra de notificacao
                        this.props.navigation.navigate('App');
                    }else{
                        this.props.navigation.navigate('Auth');
                    }
                }catch(error){//Caso ocorra falha na conexao
                    this.props.navigation.navigate('Auth');
                }
            }else{
                this.props.navigation.navigate('Auth');
            }
        } catch(e) {
          console.log(e);
        }
    }
    render(){
        return(
            <View style={styles.container}>
                <ActivityIndicator
                    color='#b5498d'
                    size='large'
                    animating={this.state.loading}
                    hidesWhenStopped={true} />
            </View>
        )
    }
}
const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({addMsg, saveLogin, setOneSignalId}, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    }
});