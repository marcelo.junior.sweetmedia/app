/** 
 * Configuração das rotas do app
 */
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';

//IMPORT DA PAGINAS DO APP
import Login from './pages/Login';
import Home from './pages/App/Home';
import DrawerScreen from './pages/DrawerScreen';
import AuthLoadingScreen from './AuthLoadingScreen';
import MessageView from './pages/App/MessageView';
import WebBrowserApp from './pages/App/WebBrowserApp';

//IMPORT STYLE
import {colors} from './pages/styles';
/*Area permitida sem autenticacao*/
const AuthStack =  createStackNavigator(
    {
        Login: Login,
        WebBrowser: WebBrowserApp,
        NotificationView: MessageView
    },
    {
        headerLayoutPreset: "center",
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor: colors.primary
            },
            headerTintColor: colors.secondary,
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        }
    }
);
/*Paginas do App*/
const MainAppStack = createStackNavigator(
    {
        Home: Home,
        MessageView: MessageView,
        WebBrowserApp : WebBrowserApp
    },
    {
        headerLayoutPreset: "center",
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor: colors.primary
            },
            headerTintColor: colors.secondary,
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        }
    }
);
/*Menu Lateral*/
const AppStack = createDrawerNavigator(
    {
        main: MainAppStack
    },
    {
        contentComponent: DrawerScreen,
        drawerBackgroundColor: '#424242',
    }
);
export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: AuthLoadingScreen,
            App: AppStack,
            Auth: AuthStack,
        },
        {
            initialRouteName: 'AuthLoading',
        }
    )
);