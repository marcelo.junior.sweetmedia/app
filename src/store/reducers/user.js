export const Types = {
    LOGIN: 'user/LOGIN',
    LOGOUT: 'user/LOGOUT',
    ADDMSG: 'user/ADDMSG',
    SETONESIGNALID: 'user/SETONESIGNALID',
    UPDATEPOINTS: 'user/UPDATEPOINTS',
};

const initialState = {
    isLogged: false,
    infoUser: {},
    onesignal_id: '',
    msgsUser: [],
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case Types.LOGIN:
            return {...state,
                isLogged: true,
                infoUser: action.payload.infoUser,
            };
        case Types.LOGOUT:
            return {...state,
                isLogged: false,
                infoUser: {},
            };
        case Types.ADDMSG:
            return {...state,
                msgsUser: action.payload.infoMsg,
            };
        case Types.SETONESIGNALID:
            return {...state,
                onesignal_id: action.payload.onesignal_id,
            };
        case Types.UPDATEPOINTS:
            const newInfoUser = {...state.infoUser};
            newInfoUser.points = action.payload.points;
            return {...state,
                infoUser: newInfoUser,
            };
        default:
            return state;
    }
}
export function saveLogin(infoUser) {
    return {
        type: Types.LOGIN,
        payload: {
            infoUser,
        },
    }
}
  
export function logout() {
    return {
        type: Types.LOGOUT,
    }
}
export function addMsg(infoMsg) {
    return {
        type: Types.ADDMSG,
        payload: {
            infoMsg,
        },
    }
}
export function setOneSignalId(onesignal_id) {
    return {
        type: Types.SETONESIGNALID,
        payload: {
            onesignal_id,
        },
    }
}
export function updatePoints(points) {
    return {
        type: Types.UPDATEPOINTS,
        payload: {
            points,
        },
    }
}