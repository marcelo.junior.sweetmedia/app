/**
 * Arquivo principal do componente userInformation
 */
//IMPORT COMPONENTES REACT
import React  from 'react';
import {View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';//FontAwesome5 foi usado por conta dos icones especificos
//IMPORT REDUX
import {connect} from 'react-redux';
//IMPORT STYLES
import styles from './styles';

const UserInformation = (props) => (
    <View style={styles.infoUser}>
        {props.user.infoUser.avatar != null ?
            (<Image 
                source={{uri: props.user.infoUser.avatar}} 
                style={styles.imageMessage}/>)
            :
            props.user.infoUser.gender === "M" ?
                (<Image 
                    source={require('../../../../images/account-avatar-male.png')} 
                    style={styles.imgUser}/>)
                :
                (<Image 
                    source={require('../../../../images/account-avatar-female.png')} 
                    style={styles.imgUser}/>)

        }
        <Text style={styles.textInfo}>{(props.user.infoUser.name) ? props.user.infoUser.name : props.user.infoUser.fullname}</Text>
        <Text style={styles.textInfo}>{props.user.infoUser.email}</Text>
        <View style={styles.scoresContainer}>
            <Icon name="shopping-cart" size={15} color="#FFFFFF"/>
            <Text style={styles.textInfo}>{props.user.infoUser.points} pontos</Text>
        </View>
    </View>
);
const mapStateToProps = state => ({
    user: state.user,
});
export default connect(mapStateToProps)(UserInformation);