import {StyleSheet} from 'react-native';
import { colors, metrics } from '../../../styles';

export default styles = StyleSheet.create({
    imgUser:{
        width: 150,
        height: 150,
        borderRadius: 75,
    },
    infoUser:{
        flex: 1,
        alignItems: 'center',
    },
    textInfo:{
        color: colors.white,
        fontWeight: 'bold',
        paddingVertical: 2,
        paddingHorizontal: 5
    },
    scoresContainer:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    scores:{
        marginLeft: metrics.smallMargin,
    }, 
});


