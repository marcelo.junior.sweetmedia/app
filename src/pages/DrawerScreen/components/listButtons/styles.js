import {StyleSheet} from 'react-native';
import { colors, metrics } from '../../../styles';

export default styles = StyleSheet.create({
    itemMenu:{
        padding: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.regular,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: metrics.baseMargin,
    },
    textItem:{
        color: colors.white,
        fontWeight: 'bold',
        marginLeft: metrics.smallMargin,
    }
});


