/**
 * Arquivo principal do componente listButtons
 */
//IMPORT COMPONENTES REACT
import React  from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';//FontAwesome5 foi usado por conta dos icones especificos
//IMPORT STYLES
import styles from './styles';

const ListButtons = (props) => (
    <View>
        <TouchableOpacity style={styles.itemMenu} onPress={props.navigateToScreen('Home')}>
            <Icon name="envelope" size={15} color="#FFFFFF"/>
            <Text style={styles.textItem}>
                Mensagens
            </Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={styles.itemMenu} 
            onPress={() => {
                props.navigation.navigate('WebBrowserApp', {link: 'https://store.sweetbonus.com.br/login'});
            }}>
            <Icon name="user" size={15} color="#FFFFFF"/>
            <Text style={styles.textItem}>
                Minha Conta
            </Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={styles.itemMenu} 
            onPress={() => {
                props.navigation.navigate('WebBrowserApp', {link: 'https://sweetbonus.com.br/blog/'});
            }}>
            <Icon name="desktop" size={15} color="#FFFFFF"/>
            <Text style={styles.textItem}>
                Acessar o blog
            </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={props.logout} style={styles.itemMenu}>
            <Icon name="sign-out-alt" size={15} color="#FFFFFF"/>
            <Text style={styles.textItem}>
                Sair
            </Text>
        </TouchableOpacity>
    </View>
);
export default ListButtons;