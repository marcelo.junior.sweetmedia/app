/**
 * Arquivo principal do componente DrawerScreen
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {ScrollView, View} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import AsyncStorage from '@react-native-community/async-storage';
//IMPORT REDUX
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {logout} from '../../store/reducers/user';
//IMPORT API
import {apiAuth} from '../../services/api';
//IMPORT COMPONENTS
import UserInformation from './components/userInformation';
import ListButtons from './components/listButtons';
//IMPORT STYLES
import styles from './styles';

class DrawerScreen extends Component {
    /*Função para fazer a navegação entre as telas*/
    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });//Configura a rota para navegação
        this.props.navigation.dispatch(navigateAction);//navega para a rota
        this.props.navigation.dispatch(DrawerActions.closeDrawer());// fecha o menu
    }
    /*Função para logout do app*/
    logout = async () => {
        try {
            const response = await apiAuth.post(`logout`,{
                email: this.props.user.infoUser.email,
                token: this.props.user.infoUser.token
            });//Deloga no redux
        } catch (error) {
            console.log(error);
        }
        try{
            await AsyncStorage.clear();//Apaga todo os dados do app da memoria do dispositivo
            this.props.navigation.navigate('Auth');//Navega para tela de login
            this.props.logout();
        }catch(error){
            console.log(error);
        }
    }

    render () {
        return (
            <View>
                <ScrollView>
                    <View style={styles.menuContainer}>
                        <UserInformation />
                        <ListButtons
                            navigation={this.props.navigation}
                            navigateToScreen={(route) => this.navigateToScreen(route)}
                            logout={async () => this.logout()}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({logout}, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(DrawerScreen)
