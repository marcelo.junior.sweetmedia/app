import {StyleSheet} from 'react-native';
import { colors, metrics } from '../styles';

export default styles = StyleSheet.create({
    menuContainer:{
        marginVertical: metrics.baseMargin,
    },
});
