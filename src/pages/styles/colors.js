const colors = {
    primary: '#60c6c5',
    secondary: '#e1e66b',
    darker: '#111',
    dark: '#424242',
    regular: '#616161',
    light: '#C0C0C0',
    lighter: '#eee',
    white: '#FFF',

    transparent: 'rgba(0, 0, 0, 0)',
};
  
export default colors;