const fonts = {
    bigger: 18,
    medium: 16,
    regular: 14,
    small: 12,
    smaller: 10,
    tiny: 8,
};
  
export default fonts;