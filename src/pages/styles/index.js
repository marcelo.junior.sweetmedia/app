/**
 * Arquivo que exporta todas os estilos padroes do app
 */
import colors from './colors';
import fonts from './fonts';
import metrics from './metrics';

export { colors, fonts, metrics };