/**
 * Arquivo principal do componente footerMsg
 */
//IMPORT COMPONENTES REACT
import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
//IMPORT STYLES
import styles from './styles';

const ErrorPage = (props) => (
  <View style={styles.errorContainer}> 
    <Icon name="exclamation-triangle" size={30} color="#b5498d"/>
    <Text style={styles.errorText}>Link expirado ou indisponível!</Text>
  </View>
);

export default ErrorPage;