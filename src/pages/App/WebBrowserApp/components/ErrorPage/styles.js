/**
 * Style padrão do componente ButtonLogin
 */
import {StyleSheet} from 'react-native';
import {colors} from '../../../../styles';

const styles = StyleSheet.create({
    errorContainer:{
        position: 'absolute', 
        flex: 1, 
        justifyContent:'center',
        alignItems:'center',
        height:'100%',
        width:'100%',
        backgroundColor: colors.white,
    },
    errorText:{
        color: colors.regular,
    }
});
export default styles;