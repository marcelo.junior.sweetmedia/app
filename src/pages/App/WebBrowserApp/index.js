/**
 * Arquivo principal da tela WebViewApp
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';
import { WebView } from 'react-native-webview';
//IMPORT REDUX
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {updatePoints} from '../../../store/reducers/user';
//IMPORT API
import {apiCustomer} from '../../../services/api';
//IMPORT COMPONENTS
import ErrorPage from './components/ErrorPage';
//IMPORT STYLE
import styles from './styles';
class WebBrowserApp extends Component {
  static navigationOptions = {//configuracao do header
    title: 'Sweet Bonus'
  }
  state={
    loading: false,
  }
  async componentWillUnmount(){
    try{
      // console.log('saindo');
      const response = await apiCustomer.post(`customers/${this.props.user.infoUser.id}`);
      if(response.data.success){//Caso ocorra sucesso na conexao
        this.props.updatePoints(response.data.customer.points);
      }
    }catch(error){
      console.log(error);
    }
  }
  showActivityIndicator = () =>{//Exibido quando o navegador esta carregando
    this.setState({loading: true});
  }
  hideActivityIndicator = () =>{//Remove a exibicao de carregamento da tela
    this.setState({loading: false});
  }
  ActivityIndicatorLoadingView = () => {
    //componente que ilustra o carregamento do navegador
    return (
      <ActivityIndicator
        color="#b5498d"
        size="large"
        style={styles.ActivityIndicatorStyle}
      />
    );
  }
  render(){
    return(
      <View
        style={this.state.loading ? styles.stylOld : styles.styleNew}>
        
        {this.state.loading ? (
          <ActivityIndicator
            color="#b5498d"
            size="large"
            style={styles.ActivityIndicatorStyle}
          />
        ) : null}
        <WebView 
          source={{uri: this.props.navigation.state.params.link}}
          onLoadStart={() => this.showActivityIndicator()}
          onLoad={() => this.hideActivityIndicator()}
          renderError={() => (
            <ErrorPage />
          )}
        />
      </View>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators({updatePoints}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WebBrowserApp);