/**
 * Arquivo principal do componente Header
 */
//IMPORT COMPONENTES REACT
import React from 'react';
import {View, Text ,TouchableOpacity, Image} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
//IMPORT REDUX
import {connect} from 'react-redux';
//IMPORT STYLES
import styles from './styles';

const Header = (props) => (
    <View style={styles.headerContainer}>
        <TouchableOpacity 
            style={styles.buttonMenu}
            onPress={() => {props.navigation.dispatch(DrawerActions.toggleDrawer())}}
        >
            <Icon name="bars" size={30} color="#e1e66b"/>
        </TouchableOpacity>
        <Text style={styles.titleHeader}>{props.user.infoUser.points} Pontos</Text>
        {props.user.infoUser.avatar != null ?
            (<Image 
                source={{uri: props.user.infoUser.avatar}} 
                style={styles.imageMessage}/>)
            :
            props.user.infoUser.gender === "M" ?
                (<Image 
                    source={require('../../../../images/account-avatar-male.png')} 
                    style={styles.imgUser}/>)
                :
                (<Image 
                    source={require('../../../../images/account-avatar-female.png')} 
                    style={styles.imgUser}/>)

        }
    </View>
);
const mapStateToProps = state => ({
    user: state.user,
});
export default connect(mapStateToProps)(Header);