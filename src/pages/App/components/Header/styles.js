/**
 * Style padrão do componente Header
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../../styles';

const styles = StyleSheet.create({
    headerContainer:{
        flexDirection: 'row',
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: colors.light,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 5,
    },
    titleHeader:{
        fontSize: fonts.bigger,
        fontWeight: 'bold',
        color: colors.secondary,
    },
    imgUser:{
        width: 30,
        height: 30,
        borderRadius: 15,
    },
});
export default styles;