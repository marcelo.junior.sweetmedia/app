/**
 * Style padrão do componente ButtonLogin
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../styles';

const styles = StyleSheet.create({
    trashMsg:{
        marginRight: 18,
    },
    messageContainer:{
        margin: 0,
        padding: 0,
        backgroundColor: colors.white,
    },
    imageContainer:{
        margin: 5,
    },
    imageMessage:{
        width: '100%',
        height: 200,
        resizeMode: 'contain'
    },
    htmlContainer:{
        margin: 0,
        padding: 0,
        justifyContent: 'flex-start',
    },
    h3:{
        textAlign: 'center',
        fontSize: fonts.medium,
        fontWeight: 'bold',
        margin: 0,
        padding: 0,
    },
    p:{
        padding: 0,
        margin: 0,
        textAlign: 'center',
    },
    strong:{
        fontWeight: 'bold',
        margin: 0,
        padding: 0,
    },
    btnContainer:{
        alignItems: 'center',
        marginVertical: metrics.baseMargin,
    },
    btnMessage:{
        width: 200,
        padding: metrics.mediumMargin,
        backgroundColor: '#00ADD8',
        alignItems: 'center',
        borderRadius: 5,
    },
    textBtnMessage:{
        fontWeight: 'bold',
        fontSize: fonts.regular,
        color: colors.white,
    }
});
export default styles;