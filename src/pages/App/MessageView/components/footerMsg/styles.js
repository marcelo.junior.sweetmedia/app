/**
 * Style padrão do componente ButtonLogin
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../../../styles';

const styles = StyleSheet.create({
    footerContainer:{
        margin: 0,
        padding: 0,
        backgroundColor: '#00ADD8',
        alignItems: 'center',
    },
    footerText:{
        color: colors.white,
        textAlign: 'center',
        marginBottom: metrics.smallMargin,
    },
    footerBold:{
        fontWeight: 'bold',
    },
    footerItalic:{
        fontStyle: "italic",
    },
    footerUnderline:{
        textDecorationLine: "underline",
    },
});
export default styles;