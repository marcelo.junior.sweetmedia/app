/**
 * Arquivo principal do componente footerMsg
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
//IMPORT STYLES
import styles from './styles';

class FooterMsg extends Component {
  render() {
    const currentDate = new Date();
    return (
      <View style={styles.footerContainer}>
        <Text
          style={[styles.footerText, styles.footerBold, styles.footerItalic]}>
          Sweet | {currentDate.getFullYear()}
        </Text>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('WebBrowserApp', {
              link: 'https://sweetbonus.com.br/politica-de-privacidade-sweet',
            });
          }}>
          <Text
            style={[
              styles.footerText,
              styles.footerItalic,
              styles.footerUnderline,
            ]}>
            | Política de privacidade |
          </Text>
        </TouchableOpacity>
        <Text style={[styles.footerText, styles.footerItalic]}>
          Todos os direitos reservados.
        </Text>
        <Text style={[styles.footerText, styles.footerBold]}>
          Você está recebendo essa mensagem por ter se cadastrado na Sweet.
        </Text>
        <Text style={[styles.footerText]}>
          Sempre que tiver algum problema nos envie um email para o endereço
          <Text style={[styles.footerText, styles.footerBold]}>
            {' '}
            contato@sweetpanels.com{' '}
          </Text>
          informando o assunto.
        </Text>
        <View>
          <Text style={[styles.footerText]}>
            A Sweet preza muito por não encher sua caixa de entrada de mensagens
            não desejadas. Por isso, você pode a qualquer momento acessar o
            portal e solicitar sua remoção da lista de mensagens.
          </Text>
        </View>
      </View>
    );
  }
}
export default FooterMsg;
