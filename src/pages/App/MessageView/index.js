/**
 * Arquivo principal da tela MessageView
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {Image, View, Text, TouchableOpacity, ScrollView, Alert } from 'react-native';
import HTMLView from 'react-native-htmlview';
import Icon from 'react-native-vector-icons/FontAwesome5';
//IMPORT REDUX
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {addMsg} from '../../../store/reducers/user';
//IMPORT API
import {apiMessages} from '../../../services/api';
//IMPORT COMPONENTES
import FooterMsg from './components/footerMsg';
//IMPORT STYLES
import styles from './styles';

class MessageView extends Component {
  static navigationOptions = ({navigation}) => {//Configura o header
    const {params = {}} = navigation.state;//recebe todos os parametros do navigation
    return{
      title: 'Sweet Bonus',//configura o title
      headerRight:(//configura o botao direito
        <TouchableOpacity 
          onPress={() => {//mostra um alert para o usuario confirmando se quer excluir a mensagem
            Alert.alert('Excluir','Tem certeza que deseja exluir a mensagem?',
              [
                {
                    text: 'Não', 
                    onPress:() => console.log('Sair'),
                },
                {
                  text: 'Sim', 
                  onPress: navigation.getParam('deleteMessage')
                }
              ],
              {cancelable: true},
            );
          }}
          style={styles.trashMsg}>
          <Icon name="trash-alt" size={25} color="#b5498d"/>
        </TouchableOpacity>)
    };
  }

  state = {//configura os estados do componente
    customers_id: this.props.navigation.state.params.message.customers_id,
    message_id: this.props.navigation.state.params.message.message_id,
    image: this.props.navigation.state.params.message.image,
    message_text: this.props.navigation.state.params.message.message_text,
    link: this.props.navigation.state.params.message.link,
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({//passa a funcao do componente para o header
      deleteMessage: this.deleteMessage,
    });
  }
  async componentDidMount(){
    /*Marca a mensagem como lida na API*/
    if(this.props.user.isLogged){//se o usuario estiver logado, pega o id do usuario e passa para o state
      this.setState({customers_id: this.props.user.infoUser.id});
    }
    try {
      const response = await apiMessages.post(`read/${this.state.customers_id}/${this.state.message_id}`);
      if(response.data.success){
        this.props.addMsg(response.data.data);//Atualiza a mensagem no redux
      }
    }catch(error){
      console.log(error);
    }
  }
  /*Deleta a mensagem*/
  deleteMessage = async () =>{
    //exclui a mensagem
    if(this.props.user.isLogged){//se o usuario estiver logado, pega o id do usuario e passa para o state
      this.setState({customers_id: this.props.user.infoUser.id});
    }
    try {
      const response = await apiMessages.delete(`destroy/${this.state.customers_id}/${this.state.message_id}`);
      if(response.data.success){// se der sucesso
        this.props.addMsg(response.data.data);//atualiza a lista no redux
        this.props.navigation.goBack();//volta para pagina home
      }
    }catch(error){
      console.log(error);
    }
  }
  render(){
    return(
      <ScrollView contentContainerStyle={styles.messageContainer}>
        {/*Conteudo da Mensagem*/}
        <View style={styles.imageContainer}>
          <Image 
            source={{uri: this.state.image}} 
            style={styles.imageMessage}/>
        </View>
        <View style={styles.htmlContainer}>
          <HTMLView
            value={this.state.message_text}
            stylesheet={styles}
            addLineBreaks={false}
          />
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity 
            style={styles.btnMessage}
            onPress={() => {
              if(this.props.user.isLogged)
                this.props.navigation.navigate('WebBrowserApp', {link: this.state.link});
              else
                this.props.navigation.navigate('WebBrowser', {link: this.state.link});
            }}>
            <Text style={styles.textBtnMessage}>Quero responder agora</Text>
          </TouchableOpacity>
        </View>
        {/*Conteudo da Mensagem*/}
        <FooterMsg navigation={this.props.navigation}/>
      </ScrollView>
    );
  }
}
const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({addMsg}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MessageView);