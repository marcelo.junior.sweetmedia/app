/**
 * Arquivo principal da tela Home
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {View} from 'react-native';
//IMPORT REDUX
import {connect} from 'react-redux';
//IMPORT STYLES
import styles from './styles';
//IMPORT COMPONENTS
import Header from '../components/Header';
import Messages from './components/Messages';

class Home extends Component {
    static navigationOptions = {
        header: null
    }
    render(){
        return (
            <View style={styles.mainContainer}>
                <Header navigation={this.props.navigation}/>
                <View style={styles.contentContainer}>
                    <Messages navigation={this.props.navigation}/>
                </View>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    user: state.user,
});
export default connect(mapStateToProps)(Home);