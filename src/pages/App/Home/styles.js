import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../styles';

const styles = StyleSheet.create({
    mainContainer:{
        flexDirection: 'column',
        flex: 1,
    },
    contentContainer:{
        flex: 1,
        backgroundColor: colors.lighter,
    }
});
export default styles;