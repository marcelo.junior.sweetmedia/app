/**
 * Style padrão do componente ButtonUpdate
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../../../styles';

const styles = StyleSheet.create({
    button:{
        alignItems:'center',
        justifyContent:'center',
        position: 'absolute',                                          
        bottom: 10,                                                    
        right: 10,
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: colors.primary,
    }
});
export default styles;