/**
 * Arquivo principal do componente mensagem
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';

export default class ButtonUpdate extends Component{
    render(){
        return(
            <TouchableOpacity 
                style={styles.button}
                onPress={this.props.update}>
                <Icon name="sync-alt" size={20} color="#e1e66b"/>
            </TouchableOpacity>
        )
    }
}