/**
 * Style padrão do componente Messages
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../../../styles';

const styles = StyleSheet.create({
    messagesContainer:{
        flex: 1,
    },
    activityIndicatorLoader:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    containerMsgVazio:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    msgVazio:{
        color: colors.regular,
        textAlign: 'center'
    },
    listMsg:{
        padding: metrics.smallMargin,
    },
    msgContainer:{
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#DDDDDD',
        padding: metrics.mediumMargin,
        marginBottom: metrics.smallMargin,
    },
    msgNew:{
        color: colors.darker,
    },
    msgTitle:{
        fontSize: fonts.bigger,
        fontWeight: 'bold',
        color: colors.light,
        textAlign: 'justify',
        lineHeight: 18,
    },
    msgDate:{
        fontSize: fonts.small,
        marginTop: metrics.smallMargin,
        color: colors.light,
    },
    msgText:{
        fontSize: fonts.regular,
        color: colors.light,
        marginTop: metrics.smallMargin,
        lineHeight: 18,
        textAlign: 'justify',
    },
});
export default styles;