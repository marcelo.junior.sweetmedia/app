/**
 * Arquivo principal do componente mensagem
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {View, FlatList, Text, TouchableOpacity, ActivityIndicator, Image} from 'react-native';
//IMPORT REDUX
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {addMsg} from '../../../../../store/reducers/user';
//IMPORT API
import {apiMessages} from '../../../../../services/api';
//IMPORT COMPONENTS
import ButtonUpdate from '../ButtonUpdate';
//IMPORT STYLE
import styles from './styles';

class Messages extends Component{
    state ={
        loading: false,//Indica que as mensagens estão sendo carregadas
    }
    componentDidMount(){
        this.getMsgs();
    }
    /*Função para buscar as mensagens na API*/
    getMsgs = async () => {
        try {
            this.setState({ loading: true });//Indica que o app esta processando as informacoes
            const response = await apiMessages.get(`search/${this.props.user.infoUser.id}`);//Busca as mensagens
            if(response.data.success){//se json indicar sucesso
                this.props.addMsg(response.data.data);//add as mensagens no redux
                this.setState({ loading: false });//Indica que o precessamento ja acabou
            }
        }catch(error){//Caso ocorra falha na conexao
            if(error.response){
                console.log(error.response);
            }else{
                console.log(error);
            }
            this.setState({ loading: false });//Indica que o precessamento ja acabou
        }
    }
    //Finaliza os processos do componente
    componentWillUnmount(){
        this.setState({ loading: false });//Indica que o precessamento ja acabou
    }
    //Renderiza cada mensagem
    renderMessage = ({item}) =>{
        const msgDate = new Date(item.created_at.replace(' ','T'));//Cria um objeto date e formata a data da mensagem conforme a data do dispositivo
        const currentDate = new Date();//Cria um objeto com a data atual
        const isValid = () => {//funcao que verifica se a mensagem ainda e valida
            if(((currentDate.getTime() - msgDate.getTime()) / 1000 / 60 / 60) >= 24)// Converte milissegundos em horas
                return false;
            else
                return true;
        } 
        return(
            <TouchableOpacity 
                style={styles.msgContainer}
                onPress={() => {
                    item.opened_at = true;
                    this.props.navigation.navigate('MessageView', {message: {customers_id: item.customers_id, ...item.body}});//Passa o id do custumer e o conteudo do body para message
                }} 
            >
                {item.opened_at == null ?
                    <Text style={[styles.msgTitle, styles.msgNew]}>
                        {isValid() ?
                            <Text>
                                <Image 
                                    source={require('../../../../../images/novo.png')}
                                    style={{width:25, height: 10,}}/>
                                <Text> </Text>
                            </Text>
                            : null
                        }
                        {item.push.title}
                    </Text>
                    :
                    <Text style={[styles.msgTitle]}>
                        {item.push.title}
                    </Text>
                }
                <Text style={[styles.msgDate, item.opened_at == null ? styles.msgNew : null]}>
                    {msgDate.getDate() +"/"+ (msgDate.getMonth()+1) +"/"+ msgDate.getFullYear()}
                </Text>
                <Text style={[styles.msgText, item.opened_at == null ? styles.msgNew : null]}>
                    {item.push.text}
                </Text>
            </TouchableOpacity>
        )
    }
    
    render(){
        return(
            <View style={this.state.loading ? styles.activityIndicatorLoader : styles.messagesContainer}>
                {this.state.loading ?
                    (
                        <ActivityIndicator
                            color='#b5498d'
                            size='large'
                            animating={this.state.loading}
                            hidesWhenStopped={true} />
                    )
                    :
                    (
                        this.props.user.msgsUser.length === 0 ?
                            <View style={styles.containerMsgVazio}>
                                <Text style={styles.msgVazio}>{this.props.user.infoUser.name.split(" ", 1)}, no momento você não tem nenhuma mensagem</Text>
                            </View>
                        :
                            <FlatList
                                contentContainerStyle={styles.listMsg} 
                                data={this.props.user.msgsUser}
                                keyExtractor={item => item.body.message_id}
                                renderItem={this.renderMessage}
                                // onEndReached={this.loadMore}
                                // onEndReachedThreshold={0.1}
                            />
                    )
                }
                <ButtonUpdate update={() => this.getMsgs()}/>
            </View>
        )
    }
}
const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({addMsg}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Messages);