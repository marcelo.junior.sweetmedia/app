/**
 * Style padrão do componente Login
 */
import {StyleSheet} from 'react-native';
import {colors} from '../styles';

const styles = StyleSheet.create({
    mainContainer:{
        flex: 1,
        backgroundColor: colors.primary,
    },
});
export default styles;