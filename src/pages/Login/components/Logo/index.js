/**
 * Arquivo principal do componente Form
 */
//IMPORT COMPONENTES REACT
import React from 'react';
import {View, Image} from 'react-native';
//IMPORT STYLES
import styles from './styles';

const Logo = () => (
    <View style={styles.imageContainer}>
        <Image 
        source={require('../../../../images/logo.png')} 
        style={styles.logo}/>
    </View>
);
export default Logo;