/**
 * Style padrão da Tela do componente Logo
 */
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    imageContainer:{
        paddingTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo:{
        width: 190,
        height: 190
    },
});
export default styles;