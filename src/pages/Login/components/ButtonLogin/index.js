/**
 * Arquivo principal do componente ButtonLogin
 */
//IMPORT COMPONENTES DO REACT
import React from 'react';
import {View, Text, TouchableOpacity, Switch, ActivityIndicator} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
//IMPORT STYLES
import styles from './styles';

const ButtonLogin = (props) => (
    <View style={styles.buttonContainer}>
        <View style={styles.rememberMe}>
            <Switch 
                onValueChange={props.toggleSwitchRemember}
                value={props.remember}
            /> 
            <Text style={styles.textRememberMe}>Permanecer conectado</Text>
        </View>
        {(props.loading) ? 
            <View style={styles.loader}>
                <ActivityIndicator
                    color='#b5498d'
                    size='large'
                    animating={props.loading}
                    hidesWhenStopped={true} />
            </View>
            :
            <LinearGradient colors={['#e276b1', '#b5498d']} style={styles.buttonEntrar}>
                <TouchableOpacity onPress={props.submitLogin}>
                    <Text style={styles.textButtonEntrar}>ENTRAR</Text>
                </TouchableOpacity>
            </LinearGradient>
        }
            
    </View>
);
export default ButtonLogin;