/**
 * Style padrão do componente ButtonLogin
 */
import {StyleSheet} from 'react-native';
import {colors, fonts, metrics} from '../../../styles';

const styles = StyleSheet.create({
    buttonContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: metrics.baseMargin,
        // marginLeft: metrics.baseMargin,
        // marginRight: metrics.baseMargin,
        marginTop: metrics.mediumMargin
    },
    rememberMe: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    textRememberMe: {
        color: colors.white,
        fontWeight: 'bold'
    },
    loader:{
        padding: 4,
        marginRight: metrics.mediumMargin,
    },
    buttonEntrar:{
        padding: 10,
        borderRadius: 5,
    },
    textButtonEntrar: {
        fontWeight: 'bold',
        color: colors.white,
        fontSize: fonts.bigger,
    },
});
export default styles;