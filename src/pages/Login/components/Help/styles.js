/**
 * Style padrão do componente Help
 */
import {StyleSheet} from 'react-native';
import {colors, metrics} from '../../../styles';

const styles = StyleSheet.create({
    suportContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginLeft: metrics.baseMargin,
        marginRight: metrics.baseMargin,
        marginBottom: metrics.mediumMargin * 2
    },
    textButtonSuport: {
        color: colors.white,
        fontWeight: 'bold',
    }
});
export default styles;