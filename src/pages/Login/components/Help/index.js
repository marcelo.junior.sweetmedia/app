/**
 * Arquivo principal do componente Form
 */
//IMPORT COMPONENTES REACT
import React  from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
//IMPORT STYLES
import styles from './styles';

const Help = (props) => (
    <View style={styles.suportContainer}>
        <TouchableOpacity
            onPress={() => {
                props.navigation.navigate('WebBrowser', {link: 'https://store.sweetbonus.com.br/password/receive'});
            }}>
            <Text style={styles.textButtonSuport}>Esqueci minha senha</Text>
        </TouchableOpacity>
        <TouchableOpacity
            onPress={() => {
                props.navigation.navigate('WebBrowser', {link: 'https://uat-sweetbonus.com.br'});
            }}>
            <Text style={styles.textButtonSuport}>Cadastrar-se</Text>
        </TouchableOpacity>
    </View>
);
export default Help;