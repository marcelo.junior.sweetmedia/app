/**
 * Style padrão do componente Form
 */
import {StyleSheet} from 'react-native';
import {colors, metrics} from '../../../styles';

const styles = StyleSheet.create({
    formContainer:{
        alignItems: 'center',
        marginLeft: metrics.baseMargin,
        marginRight: metrics.baseMargin,
    },
    inputContainer:{
        width: '100%',
        marginTop: metrics.mediumMargin,
        borderBottomWidth: 1.5,
        borderBottomColor: colors.secondary,
    },
    inputLabel: {
        color: colors.white,
        fontWeight: 'bold',
    },
    inputItens:{
        flexDirection: 'row',
        alignItems: 'center'
    },
    input:{
        width: '100%',
        padding: 1, 
        marginLeft: metrics.mediumMargin,
        color: colors.white,
        fontWeight: 'bold',
    },
    errorContainer:{
        marginTop: metrics.mediumMargin,
        backgroundColor: '#f8d7da',
        opacity:0.8,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#f5c6cb',
        width: '100%',
        padding: metrics.smallMargin,
        alignItems: 'center',
        justifyContent: 'center'    
    },
    textError:{
        color: '#721c24',
        fontWeight: 'bold',
        textAlign: "center"
    }
});
export default styles;