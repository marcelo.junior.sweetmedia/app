/**
 * Arquivo principal do componente Form
 */
//IMPORT COMPONENTES REACT
import React from 'react';
import {View, Text, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
//IMPORT STYLES
import styles from './styles';


const Form = (props) => (
    <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>E-mail</Text>
            <View style={styles.inputItens}>
                <Icon name="user" size={18} color="#FFF"/>
                <TextInput
                    style={styles.input}
                    value={props.email}
                    onChangeText={props.handleEmailChange}
                    placeholder="usuario@email.com"
                    placeholderTextColor="rgba(255,255,255,0.3)"
                    keyboardType={'email-address'}
                    returnKeyType={'next'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    onSubmitEditing={() => {this.nextInput.focus()}}
                />
            </View>
        </View>
        <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Senha</Text>
            <View style={styles.inputItens}>
                <Icon name="lock" size={18} color="#FFF"/>
                <TextInput
                    style={styles.input}
                    value={props.password}
                    onChangeText={props.handlePasswordChange}
                    ref={nextInput => this.nextInput = nextInput}
                    placeholder="**************"
                    placeholderTextColor="rgba(255,255,255,0.3)"
                    secureTextEntry={true}
                />
            </View>
        </View>
        {(props.messageError.length !== 0) ?
        <View style={styles.errorContainer}>
            <Text style={styles.textError}>{props.messageError}</Text>
        </View>
        :null}
    </View>
);
export default Form;