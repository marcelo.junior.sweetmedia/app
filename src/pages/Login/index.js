/**
 * Arquivo principal da tela de Login
 */
//IMPORT COMPONENTES REACT
import React, {Component} from 'react';
import {View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

//IMPORT REDUX
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {saveLogin} from '../../store/reducers/user';

//IMPORT STYLES
import styles from './styles';

//IMPORT COMPONENTS
import Logo from './components/Logo';
import Form from './components/Form';
import ButtonLogin from './components/ButtonLogin';
import Help from './components/Help';

//IMPORT API
import {apiAuth} from '../../services/api';

class Login extends Component {
    static navigationOptions = {
        header: null //sem header
    }
    state = {
        email: '',
        password: '',
        remember: true,// permanecer conectado ja vem selecionado por padrao
        messageError: '',// mensagem que vai ser apresentada caso occora erro
        loading: false // quando clicado no botão entrar ativa o loading
    }
    componentWillUnmount(){
        this.setState({ loading: false });//desativa o loading quando o component é desmontado
    }
    /*Função que altera o state email e messageError quando o usuario altera o input Email */
    handleEmailChange = (email) => {
        this.setState({ email, messageError: '' });
    };

    /*Função que altera o state password e messageError quando o usuario altera o input Password */
    handlePasswordChange = (password) => {
        this.setState({ password, messageError: '' });
    };

    /*Função que altera o state remember quando o usuario seleciona lembrar de mim */
    toggleSwitchRemember = (value) => {
        this.setState({ remember: value });
    }
    //Salva usuario na memoria interna para login automatico
    storeData = async (email) => {
        try {
            await AsyncStorage.removeItem('@Sweet_user');// remove algum dado ja existente
            await AsyncStorage.setItem('@Sweet_user', email);
        } catch (e) {
            console.log(e);
        }
    }

    /*Função que valida os campos do formulario e faz o login*/
    submitLogin = async () => {
        /*Verifica se algum input esta vazio */
        if(this.state.email.length === 0){
            this.setState({ messageError: 'Ops! parece que você não digitou seu email!' });
        }else if(this.state.password.length === 0) {
            this.setState({ messageError: 'Ops! parece que você não digitou sua senha!' });
        } else {//Faz a conexao com a API e valida o usuario
            try{
                this.setState({ loading: true });//Indica que o app esta processando as informacoes
                //Conecta com a api
                const response = await apiAuth.post('/login', {
                    email: this.state.email,
                    password: this.state.password,
                    onesignal_id: this.props.user.onesignal_id
                });
                if(response.data.status == 'success'){//Caso ocorra sucesso na conexao e o usuario tenha permissao para acessar o app
                    if(this.state.remember)//caso permanecer conectado for selecionado
                        this.storeData(this.state.email);// salva o email na memoria
                    
                    this.props.saveLogin(response.data.data);//salva o login no redux
                    this.props.navigation.navigate('App');//acessa o app
                }
            }catch(error){//Caso ocorra falha na conexao
                if(error.response){
                    if(error.response.status === 404 || error.response.status === 422){
                        this.setState({ messageError: 'Ops! não encontramos o usuário deste e-mail!' });
                    }else if(error.response.status === 401){
                        if(error.response.data.status == 'not_allowed_customer'){
                            this.setState({ messageError: 'Ops! você não está habilitado a usar o App, solicite acesso pelo email contato@sweetpanels.com!' });
                        }else if(error.response.data.status == 'invalid_password'){
                            this.setState({ messageError: 'Ops! a senha não confere, verifique e tente novamente!' });
                        }
                    }
                }else{
                    this.setState({ messageError: 'Ops! verifique sua conexão com a internet!' });
                }
                this.setState({ loading: false });//Indica que o precessamento ja acabou
            }
        }
    }

    render(){
        return (
            <View style={styles.mainContainer}>
                <Logo />
                <Form 
                    email={this.state.email}
                    password={this.state.password}
                    messageError={this.state.messageError}
                    handleEmailChange={(email) => this.handleEmailChange(email)}
                    handlePasswordChange={(password) => this.handlePasswordChange(password)}
                />
                <ButtonLogin
                    remember={this.state.remember}
                    loading={this.state.loading}
                    toggleSwitchRemember={(value) => this.toggleSwitchRemember(value)}
                    submitLogin={async () => this.submitLogin()}
                />
                <Help navigation={this.props.navigation}/>
            </View>
        );
    }
}  
const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch =>
    bindActionCreators({saveLogin}, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(Login);