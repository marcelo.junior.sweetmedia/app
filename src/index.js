/**
 * Arquivo que inicia o app
 */
import React, {Component} from 'react';
import BackgroundColor from 'react-native-background-color'
import OneSignal from 'react-native-onesignal';
//IMPORT REDUX
import {Provider} from 'react-redux';
import store from './store';
//IMPORT ROTAS
import Routes from './routes';
//IMPORT STYLES
import {colors} from './pages/styles';
class App extends Component {
  componentDidMount() {
    BackgroundColor.setColor(colors.primary); //Corrige o erro da imagem do SplashScreen aparecer quando o teclaso é aberto
  }
  //Remove todos os eventos ouvientes quando o componente é desmontado
  async componentWillUnmount() {//Os componentes são inseridos no arquivo AuthLoadingScreen
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }
  render(){
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
};

export default App;
