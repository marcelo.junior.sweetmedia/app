import axios from 'axios';

const apiAuth = axios.create({
  baseURL: 'https://api.sweetmedia.com.br/api/app-auth/v1/frontend/',
});
const apiMessages = axios.create({
  baseURL: 'https://api.sweetmedia.com.br/api/app-message/v1/frontend/',
});
const apiCustomer = axios.create({
  baseURL: 'https://api.sweetmedia.com.br/api/v1/frontend/',
});
export {apiAuth, apiMessages, apiCustomer};
// const getMessages = axios.create({
//   baseURL: 'https://api.uat-sweetmedia.com.br/api/messages/v1/frontend/customer-messages/',
// });
// const openedMsg = axios.create({
//   baseURL: 'https://api.uat-sweetmedia.com.br/api/messages/v1/frontend/customer-messages/opened/',
// });
// const deleteMsg = axios.create({
//   baseURL: 'https://api.uat-sweetmedia.com.br/api/messages/v1/frontend/customer-messages/delete/',
// });
// // const login = axios.create({
// //   baseURL: 'https://api.uat-sweetmedia.com.br/api/v1/customers/',
// // });
// const loginAutomatic = axios.create({
//   baseURL: 'https://api.uat-sweetmedia.com.br/api/app-auth/v1/frontend/',
// });
// // const loginAutomatic = axios.create({
// //   baseURL: 'https://api.uat-sweetmedia.com.br/api/v1/customers/',
// // });
// const logout = axios.create({
//   baseURL: 'https://api.uat-sweetmedia.com.br/api/app-auth/v1/frontend/',
// });
//export {apiAuth, loginAutomatic, logout, getMessages, openedMsg, deleteMsg};