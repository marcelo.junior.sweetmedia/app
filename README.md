# Alteração no AnimatedComponent
Necessario realizar seguinte alteração para corrigir um erro do componente

**node_modules/react-native-reanimated/src/createAnimatedComponent.js**
Altere a função abaixo:
```
componentWillMount() {
    this._attachProps(this.props);
}
```
**PARA**
```
UNSAFE_componentWillMount(){
    this._attachProps(this.props);
}
...

# Publicação na PlayStore
Nome da chave de Upload: sweetbonus-upload-key.keystore
Nome do Alias: sweetbonus-key
Senha: Sweet@2019
Duração: 25 Anos

Qual é o seu nome e o seu sobrenome?
  [Unknown]:  Sweet Bonus
Qual é o nome da sua unidade organizacional?
  [Unknown]:  Mobile
Qual é o nome da sua empresa?
  [Unknown]:  SWEET MEDIA MARKETING LTDA
Qual é o nome da sua Cidade ou Localidade?
  [Unknown]:  São Paulo
Qual é o nome do seu Estado ou Município?
  [Unknown]:  SP
Quais são as duas letras do código do país desta unidade?
  [Unknown]:  BR

Substituido do android/app/buid.gandle, para compilação do apk:
*1 Substituir*
```
debug {
    storeFile file('debug.keystore')
    storePassword 'android'
    keyAlias 'androiddebugkey'
    keyPassword 'android'
}
```
*PARA*
```
release {
    if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
        storeFile file(MYAPP_UPLOAD_STORE_FILE)
        storePassword MYAPP_UPLOAD_STORE_PASSWORD
        keyAlias MYAPP_UPLOAD_KEY_ALIAS
        keyPassword MYAPP_UPLOAD_KEY_PASSWORD
    }
}
```
*2 Remover*
```
debug {
    signingConfig signingConfigs.debug
}
```
*3 No bloco release substituir*
```
release {
    signingConfig signingConfigs.debug
    ...
}
```
*PARA*
```
release {
    signingConfig signingConfigs.release
    ...
}
```

# Contas Vinculadas

*Google play console*
*OneSignal*
*FireBase*
developers.sweetmedia@gmail.com

# JSON PARA ENVIO DA NOTIFICAÇÂO

```
{
	"app_id": "5e596715-7eb0-47a1-85e6-27cb19f81255",
	"headings": {"pt": "Teste Titulo 🎉"},
	"contents": {"en": "Teste EN", "pt": "Teste BR"},
	"android_accent_color": "0060c6c5",
	"data":{
		"message_text": "<h3>Pesquisa com pontuação elevada!!! uhuhu Mais uma vez o seu perfil foi escolhido pelos nossos parceiros para responder!</h3> <p>Basta clicar no link abaixo e responder a uma pesquisa.</p> <p><strong>Ganhe 10 pontos agora. E se você for selecionado pelos nossos parceiros você irá receber 100 pontos a pesquisa levará pouco tempo para ser concluída</strong></p> <p>A sua opinião é muito importante para nós.</p>",
		"link": "https://dkr1.ssisurveys.com/projects/boomerang?psid=rq7d5Hf1sCJDllWFULsdHcuwlD4ZewFu&sourceData=18983594",
		"image": "https://uploaddeimagens.com.br/images/002/396/751/full/unnamed_%281%29.jpg?1570210017",
		"message_id": "1020"
	},
	"include_player_ids": ["f0a1520d-bb56-447d-a200-2ef4625a93c9"]	
}
```